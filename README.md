
# realloc-test: Testing `realloc`'s corner cases.

`realloc-test` tests following `realloc (ptr, size)`'s corner cases:

 - with `ptr == nullptr` and `0 &lt;= size`
 - with `ptr == <allocated>` and `size == 0`
 - with `ptr == nullptr` and `size == 0`

# How to build

```bash
$ mkdir BUILD
$ cd BUILD
$ cmake ..
$ cmake --build .
```
