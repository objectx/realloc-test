
#include <cstdlib>
#include <iostream>
#include <iomanip>

namespace {
    template <typename T_>
        class put_hex_ {
            const T_ &  value_ ;
            size_t      width_ ;
        public:
            put_hex_ (const T_ &v, size_t width) : value_ { v }, width_ { width } {
                /* NO-OP */
            }

            std::ostream &  write (std::ostream &o) const {
                auto saved = o.setf (std::ios::hex, std::ios::basefield) ;
                auto fill = o.fill ('0') ;
                o << std::setw (width_) << value_ ;
                o.setf (saved, std::ios::basefield) ;
                return o ;
            }
        } ;

    template <typename T_>
        put_hex_<T_> put_hex (const T_ &v, size_t w) {
            return put_hex_<T_> (v, w) ;
        }

    template <typename T_>
        put_hex_<T_> put_hex (const T_ &v) {
            return put_hex_<T_> (v, 0) ;
        }

    template <typename T_>
        std::ostream &  operator << (std::ostream &o, const put_hex_<T_> &p) {
            return p.write (o) ;
        }

    void realloc_with_nullptr () {
        void *p = realloc (nullptr, 64) ;

        std::cout << "realloc (nullptr, 64) returns: 0x" << put_hex ((uintptr_t)p, 16) << std::endl ;
    }

    void realloc_with_size_0 () {
        void *p = malloc (64) ;
        void *q = realloc (p, 0) ;
        std::cout << "realloc (      x,  0) returns: 0x" << put_hex ((uintptr_t)q, 16) << std::endl ;
    }

    void realloc_with_nullptr_and_size_0 () {
        void *p = realloc (nullptr, 0) ;
        std::cout << "realloc (nullptr,  0) returns: 0x" << put_hex ((uintptr_t)p, 16) << std::endl ;
    }
}

int main () {
    realloc_with_nullptr () ;
    realloc_with_size_0 () ;
    realloc_with_nullptr_and_size_0 () ;
    return 0;
}
